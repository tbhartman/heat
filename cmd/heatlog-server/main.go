package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/google/uuid"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"
)

var debug bool
var startTime time.Time
var sessionHits int64
var sessionServerHits int64
var timezone *time.Location = time.UTC

func init() {
	startTime = time.Now()
}

// Config stores all configuration
type Config struct {
	DatabasePath string
	Serve        struct {
		Port int
		Host string
	}
	Debug bool
	Keys  []struct {
		FrontEnd uuid.UUID
		BackEnd  uuid.UUID
	}
	TLS struct {
		CertFile string
		KeyFile  string
	}
	TLSCreate certConfig
	TimeZone  string
}

func generateConfig(w io.Writer) {
	e := yaml.NewEncoder(w)
	loc, _ := time.LoadLocation("America/New_York")
	err := e.Encode(Config{
		DatabasePath: "pathToDatabase.sqlite3",
		Serve: struct {
			Port int
			Host string
		}{
			Host: "",
			Port: 8443,
		},
		Debug: false,
		Keys: []struct {
			FrontEnd uuid.UUID
			BackEnd  uuid.UUID
		}{
			{
				FrontEnd: uuid.New(),
				BackEnd:  uuid.New(),
			},
		},
		TLS: struct {
			CertFile string
			KeyFile  string
		}{
			CertFile: "path/to/cert.pem",
			KeyFile:  "path/to/key.pem",
		},
		TimeZone: loc.String(),
	})
	if err != nil {
		log.Println("Error writing new config")
		log.Fatal(err)
	}
}

func getConfig(path string) Config {
	f, err := os.Open(path)
	if err != nil {
		log.Printf("Invalid config: %s\n", path)
		log.Fatal(err)
	}
	var config Config
	func() {
		defer f.Close()
		d := yaml.NewDecoder(f)
		err = d.Decode(&config)
	}()
	if err != nil {
		log.Println("Configuration error")
		log.Fatal(err)
	}

	timezone, err = time.LoadLocation(config.TimeZone)
	if err != nil {
		log.Print("Invalid config TimeZone")
		log.Fatal(err)
	}
	return config
}

func main() {
	var configPath string
	rootCmd := &cobra.Command{
		Long: `
heatlog-server v0.0
heatlog-server is a simple web app for gathering page view data

Usage:

	heatlog-server --config <configPath>
	heatlog-server --config new

where configPath points to a valid configuration file or,
if new is given, a new config is written to stdout.
`,
		Run: func(cmd *cobra.Command, args []string) {
			if configPath == "new" {
				os.Stderr.Write([]byte("generating new config on stdout\n"))
				generateConfig(os.Stdout)
				return
			}
			fmt.Println("Starting server")

			config := getConfig(configPath)
			fmt.Printf("  configuration read from %s\n", configPath)

			if config.TLS.KeyFile == "" && config.TLSCreate.Organization != "" {
				config.TLSCreate.host = config.Serve.Host
				cert, key := generateCert(config.TLSCreate)
				cf, err := os.CreateTemp("", "heatlog_*.cert.pem")
				if err != nil {
					log.Fatal("failed to create temp cert file")
				}
				defer os.Remove(cf.Name())
				kf, err := os.CreateTemp("", "heatlog_*.key.pem")
				if err != nil {
					log.Fatal("failed to create temp key file")
				}
				defer os.Remove(kf.Name())
				_, err = io.Copy(cf, cert)
				if err != nil {
					log.Fatal("failed to write temp cert")
				}
				_, err = io.Copy(kf, key)
				if err != nil {
					log.Fatal("failed to write temp key")
				}
				config.TLS.CertFile = cf.Name()
				config.TLS.KeyFile = kf.Name()
			}

			debug = config.Debug

			var frontends []uuid.UUID
			for _, k := range config.Keys {
				frontends = append(frontends, k.FrontEnd)
			}

			s, err := NewStore(config.DatabasePath, frontends)
			if err != nil {
				log.Print("Failed to load database")
				log.Fatal(err)
			}
			fmt.Printf("  database read from %s\n", config.DatabasePath)
			defer s.Close()
			h := NewHitHandler(s, config)
			defer h.Close()

			// run server
			address := fmt.Sprintf("%s:%d", config.Serve.Host, config.Serve.Port)
			fmt.Printf("listening on %s\n", address)
			if config.TLS.CertFile != "" {
				err = http.ListenAndServeTLS(address, config.TLS.CertFile, config.TLS.KeyFile, h)
			} else {
				err = http.ListenAndServe(address, h)
			}
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	rootCmd.Flags().StringVarP(&configPath, "config", "c", "", "cache directory")
	rootCmd.MarkFlagRequired("config")
	rootCmd.Execute()
}
