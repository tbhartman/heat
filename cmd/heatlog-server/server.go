package main

import (
	"compress/gzip"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"github.com/google/uuid"
)

// LogHandler is an http.Handler that accepts the analytics REST API.
type LogHandler interface {
	http.Handler
	io.Closer
}

type logData struct {
	UUID   uuid.UUID
	Iid    int
	Time   time.Time
	Status bool
}
type systemStats struct {
	Uptime      string
	CurrentTime string
	LastHit     string
	SessionHits int64
	TotalHits   int64
}
type listData struct {
	Stats  systemStats
	Events []struct {
		Iid    int
		Time   time.Time
		Status bool
	}
}

type handler struct {
	store          Store
	keys           map[string]string
	logsToStore    chan logData
	doneProcessing sync.WaitGroup
}

func (h *handler) Close() error {
	close(h.logsToStore)
	h.doneProcessing.Wait()
	return nil
}
func (h *handler) ProcessHits() {
	h.doneProcessing.Add(1)
	go func() {
		defer h.doneProcessing.Done()
		for hit := range h.logsToStore {
			err := h.store.Log(hit)
			if err != nil {
				log.Print(err)
			}
		}
	}()
}

func (h *handler) checkCORS(res http.ResponseWriter, req *http.Request) bool {
	origin, err := url.Parse(req.Header.Get("Origin"))
	if err != nil {
		return false
	}
	// https://stackoverflow.com/a/7605119
	res.Header().Set("Access-Control-Allow-Origin", "https://"+origin.Host)
	res.Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS")
	res.Header().Set("Access-Control-Max-Age", " 1000")
	res.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization, X-Requested-With")
	return true
}

type ServeMeta struct {
	Database DatabaseMeta
	Switches []struct {
		Iid  int
		Name string
	}
	Server struct {
		Uptime    float64
		UptimeStr string
		Hits      int64
	}
}

func (h *handler) ServeHTTPMeta(res http.ResponseWriter, u uuid.UUID) {
	m, err := h.store.Meta(u)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	uptime := time.Since(startTime).Truncate(100 * time.Millisecond)
	ret := ServeMeta{
		Database: m.Database,
		Switches: m.Switches,
		Server: struct {
			Uptime    float64
			UptimeStr string
			Hits      int64
		}{
			Uptime:    uptime.Seconds(),
			UptimeStr: uptime.String(),
			Hits:      atomic.LoadInt64(&sessionServerHits),
		},
	}
	res.Header().Set("Content-Type", "application/json; charset=utf-8")
	res.Header().Set("Content-Encoding", "gzip")
	res.WriteHeader(http.StatusOK)
	gw := gzip.NewWriter(res)
	defer gw.Close()
	e := json.NewEncoder(gw)
	e.Encode(&ret)
}

func (h *handler) ServeHTTPList(res http.ResponseWriter, u uuid.UUID) {
	listing := h.store.List(u)
	res.Header().Set("Content-Type", "application/json; charset=utf-8")
	res.Header().Set("Content-Encoding", "gzip")
	res.WriteHeader(http.StatusOK)
	gw := gzip.NewWriter(res)
	defer gw.Close()
	io.WriteString(gw, "[")
	var sep = ""
	for _, l := range listing {
		var status = "false"
		if l.Status {
			status = "true"
		}
		fmt.Fprintf(gw, "%s[%d,%d,%s]", sep, l.Iid, l.Time.Unix(), status)
		sep = ","
	}
	io.WriteString(gw, "]")
}
func (h *handler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	atomic.AddInt64(&sessionServerHits, 1)
	path := strings.TrimPrefix(req.URL.Path, "/")
	if !strings.HasPrefix(path, "heat/") {
		res.WriteHeader(http.StatusForbidden)
		return
	}
	parts := strings.Split(strings.TrimPrefix(path, "heat/"), "/")
	if len(parts) < 2 {
		res.WriteHeader(http.StatusForbidden)
		return
	}
	var backendKey string
	frontUUUID := uuid.MustParse(parts[0])
	backendKey, ok := h.keys[frontUUUID.String()]
	if !ok {
		res.WriteHeader(http.StatusForbidden)
		return
	}
	switch parts[1] {
	case "log":
		if req.Header.Get("token") != backendKey || req.Method != http.MethodPut {
			res.WriteHeader(http.StatusForbidden)
			return
		}
		if len(parts) != 5 {
			res.WriteHeader(http.StatusNotAcceptable)
			return
		}
		internalId, err := strconv.Atoi(parts[2])
		if err != nil {
			res.WriteHeader(http.StatusNotAcceptable)
			return
		}
		// timestamp is relative
		timestampRel, err := strconv.ParseInt(parts[3], 10, 64)
		timestamp := time.Now().Truncate(time.Second).Add(time.Second * time.Duration(timestampRel))
		if err != nil {
			res.WriteHeader(http.StatusNotAcceptable)
			return
		}
		var status bool
		switch parts[4] {
		case "0":
		case "1":
			status = true
		default:
			res.WriteHeader(http.StatusNotAcceptable)
			return
		}
		h.logsToStore <- logData{
			UUID:   frontUUUID,
			Iid:    internalId,
			Time:   timestamp,
			Status: status,
		}
		res.WriteHeader(http.StatusCreated)
		return
	case "meta.json":
		if req.Method != http.MethodGet {
			res.WriteHeader(http.StatusForbidden)
			return
		}
		if len(parts) > 2 {
			res.WriteHeader(http.StatusNotFound)
			return
		}
		h.checkCORS(res, req)
		h.ServeHTTPMeta(res, frontUUUID)
		return
	case "log.json":
		if req.Method != http.MethodGet {
			res.WriteHeader(http.StatusForbidden)
			return
		}
		if len(parts) > 2 {
			res.WriteHeader(http.StatusNotFound)
			return
		}
		h.checkCORS(res, req)
		h.ServeHTTPList(res, frontUUUID)
		return
	default:
		res.WriteHeader(http.StatusForbidden)
	}
}

// NewHitHandler returns a HitHandler with the hit processing started.
func NewHitHandler(store Store, config Config) LogHandler {
	h := &handler{
		store:       store,
		keys:        map[string]string{},
		logsToStore: make(chan logData, 10000),
	}
	for _, v := range config.Keys {
		h.keys[v.FrontEnd.String()] = v.BackEnd.String()
	}
	h.ProcessHits()
	return h
}
