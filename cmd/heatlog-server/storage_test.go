package main_test

import (
	"database/sql"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/lithammer/dedent"
	_ "github.com/mattn/go-sqlite3"
	"github.com/olekukonko/tablewriter"
	"github.com/sergi/go-diff/diffmatchpatch"
	"github.com/stretchr/testify/require"
	main "gitlab.com/rq3/heatlog/cmd/heatlog-server"
)

func checkStrings(t *testing.T, title, received, expected string) {
	received = strings.TrimSpace(dedent.Dedent(received))
	expected = strings.TrimSpace(dedent.Dedent(expected))
	// replace text that changes
	dmp := diffmatchpatch.New()
	diffs := dmp.DiffMain(expected, received, false)
	var diffCount int
	for i, d := range diffs {
		if d.Type != diffmatchpatch.DiffEqual {
			diffCount++
			if strings.TrimSpace(d.Text) == "" {
				d.Text = strings.ReplaceAll(d.Text, " ", "∙")
				d.Text = strings.ReplaceAll(d.Text, "\n", "↩\n")
				d.Text = strings.ReplaceAll(d.Text, "\t", "⇥")
				diffs[i].Text = d.Text
			}
		} else {
			endsWithEol := d.Text[len(d.Text)-1] == '\n'
			m := strings.Split(d.Text, "\n")
			if len(m) > 4 {
				diffs[i].Text = strings.Join(m[:2], "\n") + "\n...\n" + strings.Join(m[len(m)-2:], "\n")
				if endsWithEol {
					diffs[i].Text += "\n"
				}
			}
		}
	}
	if diffCount > 0 {
		t.Errorf("differences in %s\n%v", title, dmp.DiffPrettyText(diffs))
	}
}

type dumpChecker interface {
	CheckDump(*testing.T, bool, map[string]string)
}

type scanner struct {
	str string
}

func (s *scanner) String() string {
	return s.str
}
func (s *scanner) Scan(src interface{}) error {
	switch t := src.(type) {
	case int64:
		s.str = strconv.FormatInt(t, 10)
	case float64:
		s.str = strconv.FormatFloat(t, 'g', 6, 64)
	case bool:
		if t {
			s.str = "true"
		} else {
			s.str = "false"
		}
	case []byte:
		s.str = string(t)
	case string:
		s.str = t
	case time.Time:
		s.str = t.Format(time.RFC3339)
	default:
		s.str = ""
	}
	return nil
}

func scannersToStrings(s []*scanner) []string {
	ret := make([]string, len(s))
	for i, col := range s {
		ret[i] = col.String()
	}
	return ret
}
func dumpResults(db *sql.DB, m map[string]string, name string, query string, args ...interface{}) error {
	rows, err := db.Query(query, args...)
	if err != nil {
		log.Fatal(err)
	}
	cols, _ := rows.Columns()
	var w strings.Builder
	t := tablewriter.NewWriter(&w)
	t.SetHeader(cols)
	var values []*scanner = make([]*scanner, 0)
	var scans []interface{} = make([]interface{}, 0)
	for range cols {
		s := &scanner{}
		scans = append(scans, s)
		values = append(values, s)
	}
	for rows.Next() {
		rows.Scan(scans...)
		t.Append(scannersToStrings(values))
	}
	t.SetAlignment(tablewriter.ALIGN_LEFT)
	t.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
	t.Render()
	m[name] = w.String()
	return nil
}

func dumpDb(d map[string]string, db *sql.DB) (err error) {
	check := func(e error) {
		if e != nil {
			panic(e)
		}
	}
	tableRows, _ := db.Query(`SELECT name FROM sqlite_master WHERE type = "table"`)
	var tableNames []string
	for tableRows.Next() {
		var name string
		tableRows.Scan(&name)
		if name == "sqlite_sequence" {
			continue
		}
		tableNames = append(tableNames, name)
	}
	for _, name := range tableNames {
		check(dumpResults(db, d, name, "SELECT * FROM "+name))
	}
	return
}

type storeTester struct {
	main.Store
	tmpPath string
	dump    map[string]string
}

func (s *storeTester) Close() error {
	s.Store.Close()
	defer os.Remove(s.tmpPath)

	db, _ := sql.Open("sqlite3", s.tmpPath)
	defer db.Close()

	s.dump = make(map[string]string)
	err := dumpDb(s.dump, db)
	db.Close()
	return err
}
func (s *storeTester) CheckDump(t *testing.T, ignoreExtraneousTables bool, expected map[string]string) {
	require.NotNil(t, s.dump)
	for k := range expected {
		if _, ok := s.dump[k]; !ok {
			t.Errorf("Dump does not contain expected table %s\n", k)
		}
	}
	if !ignoreExtraneousTables {
		for k := range s.dump {
			if _, ok := expected[k]; !ok {
				t.Errorf("Dump contains unexpected table %s\n", k)
			}
		}
	}
	if t.Failed() {
		return
	}
	for k, e := range expected {
		checkStrings(t, k, s.dump[k], e)
	}
}

func newStoreTester(hostnames []string) (s main.Store, d dumpChecker, err error) {
	var tf *os.File
	tf, err = ioutil.TempFile("", "gotest_*.sqlite3")
	if err != nil {
		return
	}
	tf.Close()
	subStore, err := main.NewStore(tf.Name(), hostnames)
	if err != nil {
		return
	}
	store := storeTester{
		Store:   subStore,
		tmpPath: tf.Name(),
	}
	s = &store
	d = &store
	return
}

func TestEmpty(t *testing.T) {
	r := require.New(t)
	s, d, err := newStoreTester([]string{"www.example.com"})
	r.NoError(err)
	s.Close()
	d.CheckDump(t, false, map[string]string{
		"meta": `
			+----+-----------+---------+
			| ID | TOTALHITS | LASTHIT |
			+----+-----------+---------+
			| 1  | 0         |         |
			+----+-----------+---------+`,
		"hosts": `
			+----+-----------------+
			| ID | HOSTNAME        |
			+----+-----------------+
			| 1  | www.example.com |
			+----+-----------------+`,
		"pages": `
			+----+--------+------+
			| ID | HOSTID | PATH |
			+----+--------+------+
			+----+--------+------+`,
		"dates": `
			+----+------+
			| ID | DATE |
			+----+------+
			+----+------+`,
		"hits": `
			+--------+--------+-------+
			| PAGEID | DATEID | COUNT |
			+--------+--------+-------+
			+--------+--------+-------+`,
	})
}

func TestHitPage(t *testing.T) {
	r := require.New(t)
	s, d, err := newStoreTester([]string{"www.example.com"})
	r.NoError(err)
	then, _ := time.Parse("Mon Jan 2 15:04:05 -0700 MST 2006", "Mon Jan 2 15:04:05 -0700 MST 2006")
	err = s.Hit("http://www.example.com/page1", then)
	r.NoError(err)
	err = s.Hit("http://www.example.com/page1", then)
	r.NoError(err)
	s.Close()
	d.CheckDump(t, false, map[string]string{
		"meta": `
			+----+-----------+---------------------------+
			| ID | TOTALHITS | LASTHIT                   |
			+----+-----------+---------------------------+
			| 1  | 2         | 2006-01-02T15:04:05-07:00 |
			+----+-----------+---------------------------+`,
		"hosts": `
			+----+-----------------+
			| ID | HOSTNAME        |
			+----+-----------------+
			| 1  | www.example.com |
			+----+-----------------+`,
		"pages": `
			+----+--------+--------+
			| ID | HOSTID | PATH   |
			+----+--------+--------+
			| 1  | 1      | /page1 |
			+----+--------+--------+`,
		"dates": `
			+----+------------+
			| ID | DATE       |
			+----+------------+
			| 1  | 2006-01-02 |
			+----+------------+`,
		"hits": `
			+--------+--------+-------+
			| PAGEID | DATEID | COUNT |
			+--------+--------+-------+
			| 1      | 1      | 2     |
			+--------+--------+-------+`,
	})
}

func TestInvalidDomain(t *testing.T) {
	r := require.New(t)
	s, d, err := newStoreTester([]string{"www.example.com"})
	r.NoError(err)
	err = s.Hit("http://www.example2.com/page1", time.Now())
	r.NoError(err)
	s.Close()
	d.CheckDump(t, true, map[string]string{
		"hits": `
			+--------+--------+-------+
			| PAGEID | DATEID | COUNT |
			+--------+--------+-------+
			+--------+--------+-------+`,
	})
}

func TestDBList(t *testing.T) {
	r := require.New(t)
	s, _, _ := newStoreTester([]string{"www.example.com"})
	defer s.Close()
	s.Hit("http://www.example.com/page1", time.Now())
	s.Hit("http://www.example.com/page1", time.Now())

	r.EqualValues(
		map[string]int64{
			"www.example.com/page1": 2,
		},
		s.List(),
	)
}

func TestDBMeta(t *testing.T) {
	r := require.New(t)
	s, _, _ := newStoreTester([]string{"www.example.com"})
	defer s.Close()

	// time is truncated
	n := time.Now().Truncate(time.Second)

	s.Hit("http://www.example.com/page1", n)
	s.Hit("http://www.example.com/page1", n)

	r.EqualValues(2, s.Meta().Hits)
	r.EqualValues(n.String(), s.Meta().LastHit.String())
}

func TestDBServesHost(t *testing.T) {
	r := require.New(t)
	s, _, _ := newStoreTester([]string{"www.example.com"})
	defer s.Close()

	// time is truncated
	n := time.Now().Truncate(time.Second)

	s.Hit("http://www.example.com/page1", n)
	s.Hit("http://www.example.com/page1", n)

	r.True(s.ServesHost("www.example.com"))
	r.False(s.ServesHost("www2.example.com"))
}
