package main

import (
	"database/sql"
	"io"
	"log"
	"sync"
	"time"

	"github.com/google/uuid"
	_ "github.com/mattn/go-sqlite3"
	"github.com/pkg/errors"
)

const timeStoreFmt = "2006-01-02 15:04:05"

type event struct {
	Iid    int
	Time   time.Time
	Status bool
}

// Store exposes interfaces to the database
type Store interface {
	io.Closer
	Log(logData) error
	Init() error
	List(uuid.UUID) []event
	Meta(uuid.UUID) (StorageMeta, error)
	StoresUuid(UUID uuid.UUID) bool
}

type storeStruct struct {
	path       string
	connection *sql.DB
	uuids      map[uuid.UUID]int64
	lock       sync.Mutex
}

// DatabaseMeta is the metadata from a database.
type DatabaseMeta struct {
	Events    int64
	LastEvent time.Time
}

type StorageMeta struct {
	Database DatabaseMeta
	Switches []struct {
		Iid  int
		Name string
	}
}

func (s *storeStruct) Meta(u uuid.UUID) (m StorageMeta, err error) {
	s.lock.Lock()
	defer s.lock.Unlock()
	var id int64
	var totalHits int64
	var lastHit int64
	rows, err := s.connection.Query(`SELECT * FROM meta`)
	if err != nil {
		return
	}
	defer rows.Close()
	rows.Next()
	err = rows.Scan(&id, &totalHits, &lastHit)
	if err != nil {
		return
	}
	m.Database = DatabaseMeta{
		Events:    totalHits,
		LastEvent: time.Unix(lastHit, 0),
	}

	// get uuid id
	rows, err = s.connection.Query(
		"SELECT id FROM uuids WHERE uuid = $1",
		u.String(),
	)
	if err != nil || !rows.Next() {
		return
	}
	var uuidid int
	rows.Scan(&uuidid)
	rows.Close()

	// get switches
	rows, err = s.connection.Query(
		"SELECT iid, name FROM switches WHERE uuidId = $1 ORDER BY iid ASC",
		uuidid,
	)
	if err != nil {
		return
	}
	for rows.Next() {
		var iid int
		var name string
		rows.Scan(&iid, &name)
		m.Switches = append(m.Switches, struct {
			Iid  int
			Name string
		}{Iid: iid, Name: name})
	}

	return
}
func (s *storeStruct) loadUUIDs() {
	rows, err := s.connection.Query(`SELECT * FROM uuids`)
	if err != nil {
		log.Fatal(err)
	}
	var uuids string
	var id int64
	for u := range s.uuids {
		delete(s.uuids, u)
	}
	for rows.Next() {
		rows.Scan(&id, &uuids)
		u, err := uuid.Parse(uuids)
		if err == nil {
			s.uuids[u] = id
		}
	}
}
func (s *storeStruct) StoresUuid(UUID uuid.UUID) (ok bool) {
	_, ok = s.uuids[UUID]
	return

}
func (s *storeStruct) List(u uuid.UUID) (data []event) {
	s.lock.Lock()
	defer s.lock.Unlock()

	// get uuid id
	rows, err := s.connection.Query(
		"SELECT id FROM uuids WHERE uuid = $1",
		u.String(),
	)
	if err != nil || !rows.Next() {
		return
	}
	var uuidid int
	rows.Scan(&uuidid)
	rows.Close()

	rows, err = s.connection.Query(`SELECT s.iid, e.time, e.state from events e INNER JOIN switches s ON s.id == e.switchId AND s.uuidId = $1 ORDER BY e.time DESC`, uuidid)
	if err != nil {
		return
	}
	var iid int
	var timestamp int64
	var state int
	for rows.Next() {
		err = rows.Scan(&iid, &timestamp, &state)
		if err == nil {
			data = append(data, event{
				Iid:    iid,
				Time:   time.Unix(timestamp, 0),
				Status: state == 1,
			})
		}
	}
	return
}

func (s *storeStruct) AddKey(u uuid.UUID) error {
	s.lock.Lock()
	defer s.lock.Unlock()
	var trxClose sync.Once
	trx, err := s.connection.Begin()
	if err != nil {
		return err
	}
	defer trxClose.Do(func() {
		trx.Rollback()
	})
	// get uuid id
	rows, err := trx.Query(
		"SELECT id FROM uuids WHERE uuid = $1",
		u.String(),
	)
	if err != nil || !rows.Next() {
		_, err = trx.Exec(
			"INSERT INTO uuids (uuid) VALUES ($1)",
			u.String(),
		)
	}
	rows.Close()
	trxClose.Do(func() {
		if err == nil {
			err = trx.Commit()
		} else {
			err = trx.Rollback()
		}
	})
	s.loadUUIDs()
	return err
}
func (s *storeStruct) Log(d logData) error {
	u := d.UUID
	switchInternalId := d.Iid
	timestamp := d.Time
	status := d.Status
	s.lock.Lock()
	defer s.lock.Unlock()
	var trxClose sync.Once
	trx, err := s.connection.Begin()
	if err != nil {
		return err
	}
	defer trxClose.Do(func() {
		trx.Rollback()
	})

	// get uuid id
	rows, err := trx.Query(
		"SELECT id FROM uuids WHERE uuid = $1",
		u.String(),
	)
	if err != nil || !rows.Next() {
		rows.Close()
		return errors.New("failed to find uuid")
	}
	var uuidid int
	rows.Scan(&uuidid)
	rows.Close()

	// get switch id
	tries := 0
tryselect:
	rows, err = trx.Query(
		"SELECT id FROM switches WHERE uuidId = $1 AND iid = $2",
		uuidid,
		switchInternalId,
	)
	if err != nil || !rows.Next() {
		rows.Close()
		if tries > 0 {
			return errors.New("failed to identify switch")
		}
		tries++
		trx.Exec(
			"INSERT INTO switches (uuidId, iid) VALUES ($1, $2);",
			uuidid,
			switchInternalId,
		)
		goto tryselect
	}
	var switchId int
	rows.Scan(&switchId)
	if rows.Next() {
		rows.Close()
		return errors.New("multiple switches matched")
	}
	rows.Close()

	var state int
	if status {
		state = 1
	}

	// insert event
	_, err = trx.Exec(
		"INSERT INTO events (switchId, state, time) VALUES ($1, $2, $3)",
		switchId,
		state,
		timestamp.Unix(),
	)
	if err != nil {
		return errors.Wrap(err, "failed to insert event")
	}
	trx.Exec("UPDATE meta SET totalHits = totalHits + 1 WHERE id = 1;")
	trx.Exec("UPDATE meta SET lastHitTime = unixepoch();")
	trxClose.Do(func() {
		err = trx.Commit()
	})
	return err
}

func (s *storeStruct) Init() (err error) {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.connection, err = sql.Open("sqlite3", s.path)
	if err != nil {
		return
	}

	var statement *sql.Stmt
	for _, statementText := range []string{
		`CREATE TABLE IF NOT EXISTS
		meta (
			id INTEGER PRIMARY KEY,
			totalHits INTEGER,
			lastHitTime INTEGER,
			constraint CK_meta_Locked CHECK (id=1)
		);`,
		`CREATE TABLE IF NOT EXISTS
		uuids (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			uuid TEXT,
			UNIQUE (uuid)
		);`,
		`CREATE TABLE IF NOT EXISTS
		switches (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			uuidId INTEGER NOT NULL,
			iid INTEGER NOT NULL,
			name TEXT,
			UNIQUE (uuidId, iid),
			FOREIGN KEY(uuidId) REFERENCES uuids(id)
		);`,
		`CREATE TABLE IF NOT EXISTS
		events (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			switchId INTEGER,
			state INTEGER,
			time INTEGER,
			FOREIGN KEY(switchId) REFERENCES switches(id)
		);`,
		`INSERT INTO meta SELECT 1, 0, "" WHERE NOT EXISTS (SELECT * FROM meta)`,
		`CREATE VIEW IF NOT EXISTS switchView
		AS 
		SELECT id, uuid, iid, name
		FROM switches s
			INNER JOIN uuids u ON s.uuidId = u.id
		;`,
	} {
		statement, err = s.connection.Prepare(statementText)
		if err != nil {
			return err
		}
		_, err = statement.Exec()
		if err != nil {
			return err
		}
	}

	s.loadUUIDs()
	return
}
func (s *storeStruct) Close() error {
	s.connection.Close()
	return nil
}

// NewStore creates a new database
func NewStore(path string, keys []uuid.UUID) (ret Store, err error) {
	s := &storeStruct{
		path:  path,
		uuids: make(map[uuid.UUID]int64),
	}
	err = s.Init()
	for _, k := range keys {
		s.AddKey(k)
	}
	return s, err
}
