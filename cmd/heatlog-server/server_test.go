package main_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"regexp"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	main "gitlab.com/rq3/heatlog/cmd/heatlog-server"
)

const serverPrivateKey string = "abcdefg"

type testStore struct {
	hits      map[string]int64
	lastHit   time.Time
	hostnames map[string]bool
}

func (s *testStore) Close() error {
	return nil
}
func (s *testStore) Hit(fullpath string, timestamp time.Time) error {
	_, ok := s.hits[fullpath]
	if ok {
		s.hits[fullpath]++
	} else {
		s.hits[fullpath] = 1
	}
	if s.lastHit.Before(timestamp) {
		s.lastHit = timestamp
	}
	return nil
}
func (s *testStore) Init(hostnames []string) error {
	s.hits = make(map[string]int64)
	s.hostnames = make(map[string]bool)
	for _, name := range hostnames {
		s.hostnames[name] = true
	}
	return nil
}
func (s *testStore) List() map[string]int64 {
	return s.hits
}
func (s *testStore) Meta() main.DatabaseMeta {
	var count int64
	for _, value := range s.hits {
		count += value
	}
	return main.DatabaseMeta{
		Events:    count,
		LastEvent: s.lastHit,
	}
}
func (s *testStore) ServesHost(hostname string) bool {
	ok, _ := s.hostnames[hostname]
	return ok
}

type server struct {
	Store   testStore
	once    sync.Once
	handler main.LogHandler
}

func (s *server) Init() {
	s.Store.Init([]string{"www.example.com"})
	s.handler = main.NewHitHandler(&s.Store, 1, serverPrivateKey)

}
func (s *server) ServeHTTP(r *http.Request) *http.Response {
	rec := httptest.NewRecorder()
	s.handler.ServeHTTP(rec, r)
	return rec.Result()
}
func (s *server) Close() error {
	s.once.Do(func() { s.handler.Close() })
	return nil
}

func TestOptions(t *testing.T) {
	a := assert.New(t)
	s := server{}
	s.Init()
	defer s.Close()

	req := httptest.NewRequest("OPTIONS", "http://test.example.com/hit", nil)
	req.Header.Set("Origin", "http://www.example.com")
	resp := s.ServeHTTP(req)

	s.Close()
	a.Equal(http.StatusOK, resp.StatusCode)
	a.Equal("POST, OPTIONS", resp.Header.Get("Access-Control-Allow-Methods"))
}

func TestOptionsFromOtherHost(t *testing.T) {
	a := assert.New(t)
	s := server{}
	s.Init()
	defer s.Close()

	req := httptest.NewRequest("OPTIONS", "http://test.example.com/hit", nil)
	req.Header.Set("Origin", "http://www.example2.com")
	resp := s.ServeHTTP(req)

	s.Close()
	a.Equal(http.StatusForbidden, resp.StatusCode)
}

func TestHit(t *testing.T) {
	a := assert.New(t)
	s := server{}
	s.Init()
	defer s.Close()

	req := httptest.NewRequest(
		"POST",
		"http://test.example.com/hit",
		strings.NewReader(`{"url":"https://www.example.com/page1"}`),
	)
	req.Header.Set("Origin", "http://www.example.com")
	resp := s.ServeHTTP(req)

	s.Close()
	a.Equal(http.StatusOK, resp.StatusCode)

	a.EqualValues(1, s.Store.Meta().Events)
}

func TestAnalyticsResults(t *testing.T) {
	a := assert.New(t)
	s := server{}
	s.Init()
	defer s.Close()

	// hit several times
	for i := 0; i < 3; i++ {
		for j := i; j < 3; j++ {
			req := httptest.NewRequest(
				"POST",
				"http://test.example.com/hit",
				strings.NewReader(fmt.Sprintf(`{"url":"https://www.example.com/page/%d"}`, i)),
			)
			req.Header.Set("Origin", "http://www.example.com")
			s.ServeHTTP(req)
		}
	}
	s.Close()

	req := httptest.NewRequest(
		"GET",
		fmt.Sprintf("http://test.example.com/%s/list", serverPrivateKey),
		nil,
	)
	resp := s.ServeHTTP(req)

	a.Equal(http.StatusOK, resp.StatusCode)

	expected := `
		  Uptime              : 0s                    
		  Current time        : 2006-01-02 15:04 MST  
		  Last hit            : 2006-01-02 15:04 MST  
		  Session server hits :                   10  
		  Session page hits   :                    7  
		  Total page hits     :                    6  
		
		
		+--------------------------------+-----------+
		|              PAGE              | HIT COUNT |
		+--------------------------------+-----------+
		| https://www.example.com/page/0 |         3 |
		| https://www.example.com/page/1 |         2 |
		| https://www.example.com/page/2 |         1 |
		+--------------------------------+-----------+`

	bodyBytes, _ := ioutil.ReadAll(resp.Body)
	body := string(bodyBytes)
	re := regexp.MustCompile(": 2[0-9]{3}(-[0-9]{2}){2} [0-9]{2}:[0-9]{2} \\S+")
	for _, s := range []*string{&body, &expected} {
		*s = re.ReplaceAllStringFunc(
			*s,
			func(b string) (ret string) {
				ret = ": "
				for range b[2:] {
					ret += "X"
				}
				return
			},
		)
	}
	checkStrings(
		t,
		"listing response",
		body,
		expected,
	)
}
