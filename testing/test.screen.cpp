#include "util/catch.hpp"
#include "util/Common.h"
#include "clib/u8g2.h"

#include "util/Display/TestScreen.h"
#include "util/Display/MatchesScreen.h"
#include "util/Fonts.h"
#include "util/Runner.h"
#include "util/TestBoard.h"
#include "util/ScreenJob.h"

#include "img.h"

using Display::MatchesScreen;

TEST_CASE( "My function works" ) {
    // u8g2_t u8g2;
    // u8g2_Setup_ssd1306_i2c_128x32_univision_1((u8g2_t *)&u8g2, U8G2_R0, u8x8_dummy_cb, u8x8_dummy_cb);
    // u8g2_Setup_ssd1306_i2c_128x32_univision_1((u8g2_t *)&u8g2, U8G2_R0, u8x8_dummy_cb, u8x8_dummy_cb);
    // u8x8_Setup(&u8x8, u8x8_d_ssd1306_128x32_univision, u8x8_cad_ssd13xx_fast_i2c,);
    Display::TestScreen t = Display::TestScreen::New(U8G2_R0, u8g2_Setup_ssd1306_i2c_128x32_univision_1);

    t.Update([](Display::Screen* d){
        d->SetFont(Fonts::s5x8::mf);
        d->DrawString(40, 8+4, "Hello World!");
        d->DrawString(50, 16+4, "ABCDEFG");
        d->DrawString(60, 24+4, "ABCDEFG");
        d->DrawString(70, 32+4, "ABCDEFG");
        d->DrawVline(15,0,32);
        d->DrawVline(31,0,32);
        d->DrawHline(0,15,32);
        auto r = Display::Contents::StringReader(Img::full_connection);
        d->Apply(0,0, &r);
        r = Display::Contents::StringReader(Img::high_connection);
        d->Apply(16,0, &r);
        r = Display::Contents::StringReader(Img::no_connection);
        d->Apply(0,16, &r);
    });
    CHECK_THAT(t, MatchesScreen("screen"));
}

TEST_CASE( "ScreenJob" ) {
    // u8g2_t u8g2;
    // u8g2_Setup_ssd1306_i2c_128x32_univision_1((u8g2_t *)&u8g2, U8G2_R0, u8x8_dummy_cb, u8x8_dummy_cb);
    // u8g2_Setup_ssd1306_i2c_128x32_univision_1((u8g2_t *)&u8g2, U8G2_R0, u8x8_dummy_cb, u8x8_dummy_cb);
    // u8x8_Setup(&u8x8, u8x8_d_ssd1306_128x32_univision, u8x8_cad_ssd13xx_fast_i2c,);
    TestBoard* b = new TestBoard{};
    Display::TestScreen t = Display::TestScreen::New(U8G2_R0, u8g2_Setup_ssd1306_i2c_128x32_univision_1);
    bool status = false;
    ScreenJob* sj = ScreenJob::create(&t, TimeDiff::fromMilliseconds(100), [&status](Display::Screen* d){
        d->SetFont(Fonts::s5x8::mf);
        d->DrawString(40, 8+4, status ? "on" : "off");
    });

    Runner r{b};
    r.addJob(sj);

    r.setup();
    r.loop();
    CHECK_THAT(t, MatchesScreen("screen_status_off"));
    status = true;
    sj->markScreenOutdated();
    b->delay(TimeDiff::fromMilliseconds(200));
    r.loop();
    CHECK_THAT(t, MatchesScreen("screen_status_on"));
}
