#include "util/catch.hpp"

#include "util/PubSub.h"

class MyPub : public Publisher<int> {

};


SCENARIO("pub/sub testing") {
    GIVEN("a publisher and subscriber") {
        MyPub p{};
        int value = 0;
        auto f = [&value](int arg){value=arg;};
        auto sub = p.subscribe(f);
        WHEN("I publish") {
            p.publish(2);
            THEN("value should have been updated") {
                REQUIRE(value == 2);
            }
        }
        WHEN("i unsubscribe") {
            p.publish(2);
            p.unsubscribe(sub);
            p.publish(1);
            THEN("value should not change") {
                REQUIRE(value == 2);
            }
        }
        WHEN("i disable") {
            p.publish(2);
            p.disable();
            p.publish(1);
            THEN("value should not change") {
                REQUIRE(value == 2);
            }
        }
    }
}
