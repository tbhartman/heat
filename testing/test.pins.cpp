#include "util/catch.hpp"
#include "util/Common.h"


SCENARIO("pin testing functionality") {
    GIVEN("a digital pin") {
        pinMode(1, OUTPUT);
        WHEN("i raise it") {
            digitalWrite(1, HIGH);
            THEN("it should be high") {
                REQUIRE(digitalRead(1) == HIGH);
            }
        }
        WHEN("i lower it") {
            digitalWrite(1, LOW);
            THEN("it should be low") {
                REQUIRE(digitalRead(1) == LOW);
            }
        }
    }
}
