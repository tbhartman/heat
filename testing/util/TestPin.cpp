#include "TestPin.h"

Pin::TestDigital::TestDigital(PinMode m, PinStatus s):
  DigitalIn(0, m),
  DigitalOut(0, s),
  mode(m),
  status(s)
{}

PinStatus Pin::TestDigital::Read()
{
  return status;
}
void Pin::TestDigital::Write(PinStatus newStatus)
{
  status = newStatus;
}
