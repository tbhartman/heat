#include "./TestBoard.h"
#include "./TestPin.h"

#include <assert.h>

TestBoard::TestBoard():
  time{0}
{}
TestBoard::~TestBoard()
{
  for (auto &i : pins)
  {
    delete i;
  }
}

Pin::DigitalIn TestBoard::setDigitalIn(pin_size_t id, PinMode mode)
{
  assert(pins_map.count(id) == 0);
  assert(mode != OUTPUT);
  auto ret = new Pin::TestDigital{mode, LOW};
  pins.push_back(dynamic_cast<Pin::GenericPin*>(dynamic_cast<Pin::DigitalIn*>(ret)));
  pins_map[id] = pins.size()-1;
  return *ret;
}
Pin::DigitalOut TestBoard::setDigitalOut(pin_size_t id, PinStatus initialStatus)
{
  assert(pins_map.count(id) == 0);
  auto ret = new Pin::TestDigital(OUTPUT, initialStatus);
  pins.push_back(dynamic_cast<Pin::GenericPin*>(dynamic_cast<Pin::DigitalOut*>(ret)));
  pins_map[id] = pins.size()-1;
  return *ret;
}
void TestBoard::delay(TimeDiff diff)
{
  time += diff;
}
unsigned long TestBoard::micros()
{
  return time.micros;
}

PinStatus TestBoard::getDigitalIn(pin_size_t id)
{
  assert(pins_map.count(id) == 1);
  auto pin = pins[pins_map[id]];
  assert(pin != nullptr);
  auto ppin = dynamic_cast<Pin::TestDigital*>(pin);
  assert(ppin->mode != OUTPUT);
  return ppin->Read();
}

PinStatus TestBoard::getDigitalOut(pin_size_t id)
{
  assert(pins_map.count(id) == 1);
  auto pin = pins[pins_map[id]];
  assert(pin != nullptr);
  auto ppin = dynamic_cast<Pin::TestDigital*>(pin);
  assert(ppin->mode == OUTPUT);
  return ppin->Read();
}
