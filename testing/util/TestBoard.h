#ifndef F51DFA56_0447_4DA8_B796_C01F7572A254
#define F51DFA56_0447_4DA8_B796_C01F7572A254

#include "util/Pin.h"
#include "util/Board.h"

#include <map>
#include <vector>

class TestBoard : public Board
{
    Uptime time;
    std::vector<Pin::GenericPin*> pins;
    std::map<pin_size_t, size_t> pins_map; 
  public:
    TestBoard();
    virtual ~TestBoard();
    virtual Pin::DigitalIn setDigitalIn(pin_size_t id, PinMode mode) override;
    virtual Pin::DigitalOut setDigitalOut(pin_size_t id, PinStatus initialStatus) override;
    virtual void delay(TimeDiff) override;
    virtual unsigned long micros() override;

    void incrementTime(unsigned int millis);

    PinStatus getDigitalIn(pin_size_t id);
    PinStatus getDigitalOut(pin_size_t id);
};

#endif // F51DFA56_0447_4DA8_B796_C01F7572A254
