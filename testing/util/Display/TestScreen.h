#ifndef DAB2CC75_A762_4933_BF4B_5CF49047ABB5
#define DAB2CC75_A762_4933_BF4B_5CF49047ABB5

#include "util/Display/Screen.h"

#include "TestContents.h"
#include "clib/u8g2.h"
#include <vector>
#include <string>
#include <functional>

namespace Display {

class TestScreen : public Screen
{
  public:
    TestScreen() = delete;
    TestScreen(u8g2_struct* u8g2);
    virtual ~TestScreen();
    static TestScreen New(
      const u8g2_cb_t* rotation,
      std::function<void(u8g2_t*, const u8g2_cb_t*, u8x8_msg_cb, u8x8_msg_cb)> f
    );
    Contents::Reader const* GetContents() const;
  private:
    Contents::VectorWriter contents;
    friend struct TDAccessor;
};
}


#endif // DAB2CC75_A762_4933_BF4B_5CF49047ABB5
