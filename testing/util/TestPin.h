#ifndef AD189F31_BC1D_473C_97DF_730BDDE70148
#define AD189F31_BC1D_473C_97DF_730BDDE70148

#include "util/Pin.h"
#include "TestBoard.h"

namespace Pin {

class TestDigital : public Pin::DigitalIn, public Pin::DigitalOut
{
    friend class ::TestBoard;
    PinMode mode;
    PinStatus status;
  public:
    TestDigital() = delete;
    TestDigital(PinMode, PinStatus);

    virtual PinStatus Read() override;
    virtual void Write(PinStatus) override;
};

}


#endif // AD189F31_BC1D_473C_97DF_730BDDE70148
