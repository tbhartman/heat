#include "util/catch.hpp"
#include "util/Common.h"
#include "util/Runner.h"
#include "util/Job.h"
#include "util/TestBoard.h"

class MyJob : public DelayedJob {
    int* counter;
  public:
    MyJob(int *counter):DelayedJob(), counter(counter){}
    virtual void run(TimeDiff &nextRun) override {
        (*counter)++;
    }
};

class MyRunner : public Runner {
    int* counter;
  public:
    MyRunner(Board* b, int *counter):Runner(b), counter(counter){}
    void setup(){
        MyJob* job{new MyJob{counter}};
        addJob(job);
    }
};

SCENARIO("start a runner"){
    int counter = 0;
    TestBoard* b = new TestBoard{};
    GIVEN("a digital pin") {
        MyRunner runner{b, &counter};
        runner.setup();
        WHEN("i run a loop") {
            REQUIRE(counter == 0);
            runner.loop();
            THEN("it should be low") {
                REQUIRE(counter == 1);
            }
        }
    }
}
