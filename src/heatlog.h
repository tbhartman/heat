#ifndef DF2B744F_843E_4743_93F1_90510DDFE5BA
#define DF2B744F_843E_4743_93F1_90510DDFE5BA
#include "util/Runner.h"
#include <string>

struct Token {
  Token(const char* fe, const char* be):
    front_end(fe),
    back_end(be)
  {}
  std::string front_end;
  std::string back_end;
};

struct Entry{
  Entry(const Token* token, int valve, unsigned long time, bool value):
    token(token),
    time(time),
    valve(valve),
    value(value)
  {}
  const Token* token;
  unsigned long time;
  int valve;
  bool value;
};

class HeatLog : public Runner {
  public:
    HeatLog(Board *board):Runner(board){}
    virtual void setup() override;
};


#endif // DF2B744F_843E_4743_93F1_90510DDFE5BA
