#include "util/Common.h"
#include <vector>
#include <list>
// #include <SPI.h>

// #include <ArduinoHttpClient.h>
#include <WiFiNINA.h>

#include "secrets.h"
#include "heatlog.h"

#include "img.h"
#include "util/Job.h"
#include "util/Display/Screen.h"
#include "util/Display/Contents.h"
#include "util/ScreenJob.h"
#include "util/Board.h"
#include "util/PubSub.h"

#include <U8g2lib.h>
// #include "src/util/Board.h"
#include "util/Fonts.h"

//#include "arduino_secrets.h"

// #include <string>

// arduino pins
const pin_size_t D2 = 2;
const pin_size_t D3 = 3;
const pin_size_t D4 = 4;
const pin_size_t D5 = 5;
const pin_size_t D6 = 6;
const pin_size_t D7 = 7;
const pin_size_t D8 = 8;
const pin_size_t D9 = 9;
const pin_size_t D10 = 10;
const pin_size_t D11 = 11;
// const pin_size_t D12 = 12;
const pin_size_t D21 = 21;
const pin_size_t D20 = 20;

const pin_size_t FURNACE_PIN = D2;
const pin_size_t DHW_PIN = D3;
const pin_size_t ZONE_1_PIN = D4;
const pin_size_t ZONE_2_PIN = D5;
const pin_size_t ZONE_3_PIN = D6;
const pin_size_t ZONE_4_PIN = D7;
const pin_size_t ZONE_5_PIN = D8;

const pin_size_t UPLOAD_PIN = D9;
const pin_size_t STATUS_PIN = D10;
const pin_size_t ERROR_PIN = D11;

const pin_size_t BUTTON_PIN = D20;
const pin_size_t TEST_PIN = D21;



std::vector<Entry> queue;

class InputPin {
  const pin_size_t pid;
  const bool pullup;
  bool value;
  bool flipped;
public:
  InputPin(pin_size_t pid, bool pullup):
    pid(pid),
    pullup(pullup),
    value(false)
  {}
  void setup() {
    pinMode(pid, pullup ? INPUT_PULLUP : INPUT_PULLDOWN);
  }
  void update() {
    switch(digitalRead(pid)) {
      case LOW:
        flipped = pullup ? !value : value;
        value = pullup;
        break;
      case HIGH:
        flipped = pullup ? value : !value;
        value = !pullup;
        break;
      default:
        break;
    }
  }
  bool getValue() {return value;}
  bool changed() {return flipped;}
};

InputPin testBut(TEST_PIN, true);

class ValvePin : public InputPin {
  int valveId;
  bool started;
  public:
  ValvePin(pin_size_t pid, int vid):
    InputPin(pid, true),
    valveId(vid),
    started(false)
  {}
  void updateQueue() {
    if (this->changed() || !started) {
      started = true;
      queue.push_back(Entry(testBut.getValue() ? &TEST_TOKEN : &PROD_TOKEN, valveId, millis(), this->getValue()));
    }
  }
};

InputPin button(BUTTON_PIN, true);
ValvePin furnace(FURNACE_PIN, 10);
ValvePin dhw(DHW_PIN, 11);
std::vector<ValvePin*> zones{
  new ValvePin(ZONE_1_PIN, 1),
  new ValvePin(ZONE_2_PIN, 2),
  new ValvePin(ZONE_3_PIN, 3),
  new ValvePin(ZONE_4_PIN, 4),
  new ValvePin(ZONE_5_PIN, 5)
};

int name = 0;
int lastWrite = 0;

// const int WL_CONNECTED=0;
// const int WL_IDLE_STATUS=0;
// class WiFiC{
//   public:
//   static int begin(const char*, const char*) {}
//   static long RSSI(){}
// };
// WiFiC WiFi{};
// class WiFiSSLClient{
//   public:
//   const char* read(){}
//   bool connectSSL(const char*, int){}
//   bool connected(){}
//   bool available(){}
//   void print(const char* c=nullptr){}
//   void println(const char* c=nullptr){}
// };

WiFiSSLClient client;

class StatusLight : public DelayedJob {
    pin_size_t pin;
    TimeDiff on;
    TimeDiff off;
    TimeDiff offset;
    bool state;
  public:
    StatusLight(pin_size_t pin, TimeDiff on, TimeDiff off, int64_t offset = 0):
      pin(pin),
      on(on),
      off(off),
      offset{offset},
      state(false)
    {}
    virtual void run(TimeDiff &nextRun) override {
      if (offset.micros != 0) {
        digitalWrite(pin, LOW);
        nextRun.micros += offset.micros;
        offset.micros = 0;
        return;
      }
      digitalWrite(pin, state ? LOW : HIGH);
      nextRun = state ? off : on;
      state = !state;
    }
};

StatusLight* statusLight = new StatusLight{STATUS_PIN, TimeDiff::fromMilliseconds(500), TimeDiff::fromMilliseconds(500)};
StatusLight* statusLight2 = new StatusLight{ERROR_PIN, TimeDiff::fromMilliseconds(1000), TimeDiff::fromMilliseconds(1000)};
StatusLight* statusLight3 = new StatusLight{UPLOAD_PIN, TimeDiff::fromMilliseconds(250), TimeDiff::fromMilliseconds(250)};

u8g2_struct* u8g2;

// U8G2_SSD1306_128X32_UNIVISION_1_HW_I2C u8g2(U8G2_R0, U8X8_PIN_NONE);

class ScreenLineWriter : public LineWriter {
    std::list<std::string> vs;
    Display::Screen* s;
    Board* b;
  public:
    ScreenLineWriter(Display::Screen* s, Board* b):
      s(s),
      b(b)
    {}
    bool operator!(){return false;}
    size_t println(const char* a ){ return print(a); }
    size_t println(char a ){ return print(a); }
    size_t println(unsigned char a ){ return print(a); }
    size_t println(int a ){ return print(a); }
    size_t println(unsigned int a ){ return print(a); }
    size_t println(long a ){ return print(a); }
    size_t println(unsigned long a ){ return print(a); }
    size_t println(long long a ){ return print(a); }
    size_t println(unsigned long long a ){ return print(a); }
    size_t println(double a ){ return print(a); }
    size_t println(void){ return 0; }
    size_t print(const char* a ){ std::string str{a}; print(str); }
    size_t print(char a ){ std::string str{a}; print(str); }
    size_t print(unsigned char a ){ std::string str = std::to_string(a); print(str); }
    size_t print(int a ){ std::string str = std::to_string(a); print(str); }
    size_t print(unsigned int a ){ std::string str = std::to_string(a); print(str); }
    size_t print(long a ){ std::string str = std::to_string(a); print(str); }
    size_t print(unsigned long a ){ std::string str = std::to_string(a); print(str); }
    size_t print(long long a ){ std::string str = std::to_string(a); print(str); }
    size_t print(unsigned long long a ){ std::string str = std::to_string(a); print(str); }
    size_t print(double a ){ std::string str = std::to_string(a); print(str); }
    size_t print(void){ return 0; }
    void print(std::string str) {
      if (vs.size() > 2) {
        vs.pop_back();
      }
      vs.push_front(str);
      s->Update([this](Display::Screen *s)->void{
        s->SetFont(Fonts::s5x8::mf);
        int n = 0;
        for (auto i = this->vs.begin(); i != this->vs.end(); ++i)
        {
          s->DrawString(2, (2+8)*((n++)+1), i->c_str());
        }
        
      });
      b->delay(200000);
    }
};

void HeatLog::setup() {
  // board->delay(1000);
  // while (!*writer){}
  // writer->println("starting");
  // writer->println("starting display");
  // u8g2.begin();
  // for (int i = 20; i >= 0; i--) {
  //   u8g2.firstPage();
  //   do {
  //     u8g2.setFont(u8g2_font_6x10_mf);
  //     String s(i);
  //     String count("Countdown: ");
  //     count += s;
  //     u8g2.drawStr(4,15,count.c_str());
  //   } while ( u8g2.nextPage() );
  //   board->delay(50);
  // }
  // board->delay(1000);
  auto u = new U8G2_SSD1306_128X32_UNIVISION_1_HW_I2C{U8G2_R0, U8X8_PIN_NONE};
  u->begin();
  Display::Screen* s = new Display::Screen{u->getU8g2()};
  s->PowerOn();
  auto b = this->board;
  auto slw = new ScreenLineWriter(s, b);
  // LineWriter::set(slw);

  s->Update([](Display::Screen *s)->void{
    s->SetFont(Fonts::s5x8::mf);
    s->DrawString(40, 8+4, "Hello World!");
    s->DrawString(50, 16+4, "ABCDEFG");
    s->DrawString(60, 24+4, "ABCDEFG");
    s->DrawString(70, 32+4, "ABCDEFG");
    s->DrawVline(15,0,32);
    s->DrawVline(31,0,40);
    s->DrawHline(0,15,32);
    auto r = Display::Contents::StringReader(Img::full_connection);
    s->Apply(0,0, &r);
    r = Display::Contents::StringReader(Img::high_connection);
    s->Apply(16,0, &r);
    r = Display::Contents::StringReader(Img::no_connection);
    s->Apply(0,16, &r);
    r = Display::Contents::StringReader(Img::failed_connection);
    s->Apply(16,16, &r);
  });

  LineWriter::get()->println("test");

  ScreenJob* sj;
  sj = ScreenJob::create(s, TimeDiff::fromMilliseconds(500), [](Display::Screen *s)->void{
    auto writer = LineWriter::get();
    writer->println("screen job start");
    s->SetFont(Fonts::s5x8::mf);
    s->DrawVline(15,0,32);
    s->DrawVline(31,0,32);
    s->DrawHline(0,15,32);
    {
      // wifi status
      long rssi = 1;
      const char* wifi_icon = Img::no_connection;
      const char* wifi_status = nullptr;
      switch (WiFi.status()) {
        case WL_NO_MODULE:
          wifi_status = wifi_status == nullptr ? "NO_MODULE" : wifi_status;
        case WL_IDLE_STATUS:
          wifi_status = wifi_status == nullptr ? "IDLE_STATUS" : wifi_status;
        case WL_NO_SSID_AVAIL:
          wifi_status = wifi_status == nullptr ? "NO_SSID_AVAIL" : wifi_status;
          wifi_icon = Img::failed_connection;
          break;
        case WL_SCAN_COMPLETED:
          wifi_status = wifi_status == nullptr ? "SCAN_COMPLETED" : wifi_status;
        case WL_CONNECT_FAILED:
          wifi_status = wifi_status == nullptr ? "CONNECT_FAILED" : wifi_status;
        case WL_CONNECTION_LOST:
          wifi_status = wifi_status == nullptr ? "CONNECTION_LOST" : wifi_status;
        case WL_DISCONNECTED:
          wifi_status = wifi_status == nullptr ? "DISCONNECTED" : wifi_status;
        case WL_AP_FAILED:
          wifi_status = wifi_status == nullptr ? "AP_FAILED" : wifi_status;
          break;
        case WL_AP_LISTENING:
          wifi_status = wifi_status == nullptr ? "AP_LISTENING" : wifi_status;
        case WL_AP_CONNECTED:
          wifi_status = wifi_status == nullptr ? "AP_CONNECTED" : wifi_status;
        case WL_CONNECTED:
          wifi_status = wifi_status == nullptr ? "CONNECTED" : wifi_status;
          rssi = WiFi.RSSI();
      }
      wifi_status = wifi_status == nullptr ? "unknown" : wifi_status;
      if (rssi <= 0) {
        if (rssi >= -30) {
          wifi_icon = Img::full_connection;
        } else if (rssi >= -50) {
          wifi_icon = Img::high_connection;
        } else if (rssi >= -90) {
          wifi_icon = Img::low_connection;
        } else {
          wifi_icon = Img::zero_signal;
        }
      }
      auto r = Display::Contents::StringReader(wifi_icon);
      s->Apply(0,0, &r);
      s->DrawString(40, 8+4, wifi_status);
      std::string ss = std::to_string(rssi);
      s->DrawString(40, 8+20, ss.c_str());
    }
  });

  DelayedJob* refreshscreen = DelayedJob::create([sj](TimeDiff &next)->void{
    next = TimeDiff::fromMilliseconds(100);
    sj->markScreenOutdated();
  });
  DelayedJob* wifijob = DelayedJob::create([sj, b, &WiFi](TimeDiff &next)->void{
    static int old_status = -1;
    static Uptime previous_update{0};
    auto current_time = b->uptime();
    if (previous_update.micros == 0) {
      if (current_time.seconds() < 1) {
        next = TimeDiff::fromMilliseconds(100);
      } else {
        WiFi.begin(WIFI_SSID, WIFI_PASSPHRASE);
        previous_update = current_time;
      }
      return;
    }
    if (WiFi.status() == old_status && (current_time - previous_update) < TimeDiff::fromSeconds(2)) {
      next = TimeDiff::fromSeconds(2);
      return;
    }
    if (WiFi.status() != WL_CONNECTED && (current_time - previous_update) > TimeDiff::fromSeconds(2)) {
      WiFi.disconnect();
      WiFi.begin(WIFI_SSID, WIFI_PASSPHRASE);
    }
    old_status = WiFi.status();
    previous_update = current_time;
    next = TimeDiff::fromMilliseconds(300);
    return;
  });
  LineWriter::get()->println("addjob");
  this->addJob(refreshscreen);
  this->addJob(sj);
  LineWriter::get()->println("adding wifi job");
  this->addJob(wifijob);


  // while (status != WL_CONNECTED) {
  //   // Serial.print("Attempting to connect to network: ");
  //   // Serial.println(WIFI_SSID);
  //   // Connect to WPA/WPA2 network:

  //   // wait 10 seconds for connection:
  //   delay(5000);
  // }

  this->addJob(statusLight);
  this->addJob(statusLight2);
  this->addJob(statusLight3);
/*
  // you're connected now, so print out the data:
Serial.println("You're connected to the network");
Serial.println("---------------------------------------");
Serial.println("Board Information:");
  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print your network's SSID:
  Serial.println();
  Serial.println("Network Information:");
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.println(rssi);
  Serial.println("---------------------------------------");
*/
  // furnace.setup();
  // dhw.setup();
  // for (auto i:zones) {
  //   i->setup();
  // }
  // button.setup();
  // testBut.setup();

  LineWriter::get()->println("setting light pin modes");
  pinMode(STATUS_PIN, OUTPUT);
  pinMode(ERROR_PIN, OUTPUT);
  pinMode(UPLOAD_PIN, OUTPUT);

  // digitalWrite(STATUS_PIN, LOW);
  // digitalWrite(ERROR_PIN, LOW);
  // digitalWrite(UPLOAD_PIN, LOW);

  /*
  digitalWrite(RED, HIGH);
  delay(1000);
  digitalWrite(GREEN, HIGH);
  delay(1000);
  digitalWrite(BLUE, HIGH);
  delay(1000);
  digitalWrite(RED, LOW);
  digitalWrite(GREEN, LOW);
  digitalWrite(BLUE, LOW);
  */
}



// timer interrupt
//    read current statuses
//    add to queue if changed
//    update changeflags to be used in loop
// main loop
//    process queue (limit to N per cycle? N=1?)
//    check change flags
//      did the button get pushed so i need to turn on display?
//      do i need to flash the status light?
//      do i need to show the error light?

// int64_t last = 0;
// bool value = false;
// bool process = false;
// bool made_request = false;
// void HeatLog::innerLoop() {
//   if ((value && millis() - last > 100) || (!value && millis() - last > 3000)) {
//     digitalWrite(STATUS_PIN, value ? LOW : HIGH);
//     value = !value;
//     last = millis();
//   }
//   delay(value ? 25 : 500);

//   furnace.update();
//   dhw.update();
//   for (auto i:zones) {
//     i->update();
//   }
//   button.update();
//   testBut.update();

//   furnace.updateQueue();
//   dhw.updateQueue();
//   for (auto i:zones) {
//     i->updateQueue();
//   }


//   digitalWrite(ERROR_PIN, queue.size() > 0 ? HIGH : LOW);
//   if (button.changed() && button.getValue()) process = !process;
//   if (queue.size() > 0 && process && !made_request) {
//     // long rssi = WiFi.RSSI();
//     // Serial.print("signal strength (RSSI):");
//     // Serial.println(rssi);
//     // Serial.print("queue length: ");
//     // Serial.println(queue.size());
//     digitalWrite(UPLOAD_PIN, HIGH);
//     if (!client.connected()) {
//       if (client.connectSSL("documen.twith.in", 443)) {
//         // Serial.println("connected to server");
//       } else {
//         // Serial.println("failed to connect");
//       }
//     }
//     if (client.connected()) {
//       std::string path("PUT /heat/");
//       path += queue[0].token->front_end;
//       path += "/log/";
//       path += std::to_string(queue[0].valve);
//       path += "/";
//       signed long long offset = ((signed long long)(queue[0].time) - (signed long long)(millis())) / 1000;
//       path += std::to_string((int)offset);
//       path += "/";
//       path += queue[0].value ? "1" : "0";
//       path += " HTTP/1.1";
//       // Make a HTTP request:
//       // Serial.println(path.c_str());
//       client.println(path.c_str());
//       client.println("Host: www.example.com");
//       client.print("token: "); client.println (TEST_TOKEN.back_end.c_str());
//       client.println("Connection: keep-alive");
//       client.println();
//       made_request = true;
//     } 
//   }
//   if (made_request && client.available()) {
//     bool ok = false;
//     std::string result;
//     while (client.available()) {
//       result += client.read();
//       if (result.find("201 Created") != std::string::npos) {
//         ok = true;
//       }
//       if (result.size() > 100) {
//         // Serial.print(result.substr(0, 50).c_str());
//         result = result.substr(50);
//       }
//     }
//     // Serial.print(result.c_str());
//     // Serial.print("RESULT: ");
//     // Serial.println(ok ? "OK" : "KO");
//     if (ok) {
//       queue.erase(queue.begin());
//     }
//     digitalWrite(UPLOAD_PIN, LOW);
//     made_request = false;
//   }
//   // if(buttonFlipped || millis() < 2000) {
//   //   if (buttonFlipped) {
//   //     button = !button;
//   //     if (button) {
//   //       name++;
//   //     }
//   //   }
//   //   digitalWrite(LED, button || millis() < 1000 ? HIGH : LOW);
//   // }
//   // // write to screen
//   // if (millis() - lastWrite > 500 || lastWrite == 0 || buttonFlipped) {
//   //   String nameStr;
//   //   if (name > 10) {
//   //     name = 0;
//   //   }
//   //   switch(name){
//   //     case 0:
//   //       nameStr = String("Tim");
//   //       break;
//   //     case 1:
//   //       nameStr = String("Andrea");
//   //       break;
//   //     case 2:
//   //       nameStr = String("Piper");
//   //       break;
//   //     case 3:
//   //       nameStr = String("Ryas");
//   //       break;
//   //     case 4:
//   //       nameStr = String("Kella");
//   //       break;
//   //     case 5:
//   //       nameStr = String("Lilias");
//   //       break;
//   //     case 6:
//   //       nameStr = String("Silas");
//   //       break;
//   //     case 7:
//   //       nameStr = String("Titus");
//   //       break;
//   //     case 8:
//   //       nameStr = String("Lenny");
//   //       break;
//   //     case 9:
//   //       nameStr = String("Kevin");
//   //       break;
//   //     case 10:
//   //       nameStr = String("Jonathon");
//   //       break;
//   //   }
//   //   u8g2.firstPage();
//   //   do {
//   //     u8g2.setFont(u8g2_font_6x10_mf);
//   //     u8g2.drawStr(4,15,String("Hello " + nameStr + "!").c_str());
//   //     String s(millis() / 1000);
//   //     s += "s";
//   //     u8g2.setFont(u8g2_font_5x7_mf);
//   //     s = String("uptime: " + s );
//   //     u8g2.drawStr(4,30,s.c_str());
//   //     u8g2.
//   //     lastWrite = millis();
//   //   } while ( u8g2.nextPage() );
//   // }
// }
