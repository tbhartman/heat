#ifndef D32EA722_1316_411A_AB28_E0C7236095B8
#define D32EA722_1316_411A_AB28_E0C7236095B8
#include "./Common.h"

/*
a means of interacting with pins, etc. in a way that i would
be able to mock
*/

namespace Pin {

class GenericPin
{
  public:
    virtual ~GenericPin() = default;
};

class DigitalIn : public GenericPin
{
    pin_size_t id;
  public:
    DigitalIn() = delete;
    DigitalIn(pin_size_t, PinMode);
    virtual ~DigitalIn() = default;

    virtual PinStatus Read();
};

class DigitalOut : public GenericPin
{
    pin_size_t id;
  public:
    DigitalOut() = delete;
    DigitalOut(pin_size_t, PinStatus);
    virtual ~DigitalOut() = default;

    virtual void Write(PinStatus);
};

}


#endif // D32EA722_1316_411A_AB28_E0C7236095B8
