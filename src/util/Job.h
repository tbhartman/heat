#ifndef F44DC622_6EAA_43EA_94C4_246B75CC0379
#define F44DC622_6EAA_43EA_94C4_246B75CC0379

#include "./VirtualBase.h"
#include "Uptime.h"

#include <stdint.h>
#include <functional>

class Job : public VirtualBase {
  public:
    virtual bool runNext() = 0;
    virtual Uptime run(const Uptime& currentTime) = 0;
};

class EveryLoopJob : public Job {
  public:
    virtual bool runNext() override { return true; }
    virtual Uptime run(const Uptime& currentTime) override { run(); return 0; }
    virtual void run() = 0;
    static EveryLoopJob *create(std::function<void()> f);
};

class DelayedJob : public Job {
  public:
    virtual bool runNext() override final { return false; }
    virtual Uptime run(const Uptime& currentTime) override final {
        TimeDiff delay{0};
        run(delay);
        return currentTime + delay;
    }
    virtual void run(TimeDiff &nextRun) = 0;
    static DelayedJob *create(std::function<void(TimeDiff &)> f);
};

class IntervalJob : public DelayedJob {
    TimeDiff interval;
    using DelayedJob::run;

public:
    IntervalJob(TimeDiff interval):
      interval{interval}
    {}

    virtual void run(TimeDiff &nextRun) override final {
        run();
        nextRun = interval;
    }
    virtual void run() = 0;

    static IntervalJob *create(TimeDiff interval, std::function<void()> f);
};


#endif // F44DC622_6EAA_43EA_94C4_246B75CC0379
