// #include <Arduino.h>

#ifndef EA708654_A581_4EB8_A804_82141FEF2366
#define EA708654_A581_4EB8_A804_82141FEF2366
#include <cstdint>
#include <functional>

#include "../VirtualBase.h"

struct u8g2_struct;

namespace Display {

namespace Contents {
  class Reader;
}

class Screen;

typedef void (*Updater)(Screen*);

class Screen : public VirtualBase
{
  public:
    Screen(u8g2_struct *u8g2);
    Screen(const Screen& other);
    Screen& operator=(Screen& other) noexcept;
    virtual ~Screen();
    /*
    template<typename T>
    static Screen New(const u8g2_cb_t *rotation, uint8_t reset) {
      static_assert(std::is_base_of<U8G2, T>::value, "T must inherit from U8G2");
      return Screen{new T(rotation, reset)};
    }
    */

    // iterator for range-based for loops
    class Iterator {
        Screen* display;
      public:
        Iterator()=delete;
        Iterator(Screen* display);
        Iterator(const Iterator& other);
        Iterator& operator=(Iterator& other) noexcept;
        ~Iterator() = default;

        Screen const & operator*() const;
        Iterator const & operator++();
        bool operator!=(Iterator const&) const;
    };
    inline Iterator begin() {
        return Iterator(this);
    }
    inline Iterator end() {
        return Iterator(nullptr);
    }

  private:
    void FirstPage();
    bool NextPage();
  public:
    // void Update(Updater);
    void Update(std::function<void(Screen*)>);
    void Apply(uint16_t x, uint16_t y, Contents::Reader* r);

    void PowerOff();
    void PowerOn();

    void SetFont(const uint8_t* fontName);
    void SetColor(bool);
    void DrawString(uint16_t x, uint16_t y, const char* content);
    void DrawPixel(uint16_t x, uint16_t y);
    void DrawHline(uint16_t x, uint16_t y, uint16_t length);
    void DrawVline(uint16_t x, uint16_t y, uint16_t length);
  protected:
    u8g2_struct* u8g2;
};

}


#endif // EA708654_A581_4EB8_A804_82141FEF2366
