#ifndef B64D236E_356C_4734_9469_4E1CBD61D8B0
#define B64D236E_356C_4734_9469_4E1CBD61D8B0

#include "../VirtualBase.h"
#include <cstdint>
#include <memory>

namespace Display {
namespace Contents {

class Reader : public VirtualBase
{
  public:
    virtual uint16_t Width() const = 0;
    virtual uint16_t Height() const = 0;
    virtual bool At(int x, int y) const = 0;
};

class Writer : public Reader
{
  public:
    virtual void Resize(int width, int height) = 0;
    virtual void Clear() = 0;
    virtual void Set(int x, int y, bool value) = 0;
};

class StringReader : public Reader
{
    const char *ptr;
  public:
    StringReader();
    StringReader(const char*);
    virtual ~StringReader() {};
    StringReader(const StringReader&s) {ptr = s.ptr;}
    StringReader(StringReader&&s) {auto a = s.ptr; s.ptr = ptr; ptr = a;}
    StringReader& operator = (const StringReader&s) {ptr = s.ptr; return *this;}
    StringReader& operator = (StringReader&&s) {auto a = s.ptr; s.ptr = ptr; ptr = a; return *this;}

    virtual uint16_t Width() const override;
    virtual uint16_t Height() const override;
    virtual bool At(int x, int y) const override;
};

}
}


#endif // B64D236E_356C_4734_9469_4E1CBD61D8B0
