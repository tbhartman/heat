#include "ScreenJob.h"
#include "Common.h"

ScreenJob::ScreenJob(Display::Screen* screen, TimeDiff minimumDelay):
  screen(screen),
  screenOutOfDate(true),
  minimumDelay(minimumDelay)
{
}
void ScreenJob::markScreenOutdated()
{
  screenOutOfDate = true;
}
void ScreenJob::run(TimeDiff &nextRun)
{
  if (screenOutOfDate)
    for (auto i = screen->begin(); i != screen->end(); ++i)
      this->updateScreen(screen);
  nextRun = minimumDelay;
};
namespace
{
class ScreenFuncJob : public ScreenJob {
    std::function<void(Display::Screen*)> f;
  public:
    ScreenFuncJob(Display::Screen* screen, TimeDiff minimumDelay, std::function<void(Display::Screen*)> f):
        ScreenJob(screen, minimumDelay),
        f(f)
    {}
    virtual void updateScreen(Display::Screen* s) final {
        f(s);
    }
};
}
ScreenJob *ScreenJob::create(Display::Screen* screen, TimeDiff minimumDelay, std::function<void(Display::Screen*)> f) {
    return new ScreenFuncJob{screen, minimumDelay, f};
}
