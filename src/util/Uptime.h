#ifndef DF9DCAD8_CC3D_4F7A_B4C7_F1A707FD7367
#define DF9DCAD8_CC3D_4F7A_B4C7_F1A707FD7367

#include <stdint.h>
#include "Common.h"

struct TimeDiff;

struct Uptime {
    Uptime(uint64_t micros):
      micros(micros)
    {}
    Uptime& operator=(const Uptime &a) { micros = a.micros; return *this; }
    uint64_t micros;
    double seconds() { return double(micros) / 1000000; }
    double minutes() { return seconds() / 60; }
    double hours() { return minutes() / 60; }
    double days() { return hours() / 24; }
    TimeDiff operator- (const Uptime &b)const;
    Uptime& operator+=(const TimeDiff &b);
    Uptime operator+ (const TimeDiff &b) const;
};

struct TimeDiff {
    static TimeDiff fromSeconds(int64_t s) {
        return TimeDiff::fromMilliseconds(s * 1000);
    }
    static TimeDiff fromMilliseconds(int64_t ms) {
        return TimeDiff{ms * 1000};
    }
    TimeDiff(int64_t micros):
        micros(micros)
    {}
    TimeDiff& operator=(const TimeDiff &a) {
        micros = a.micros;
        return *this;
    }
    int64_t micros;

    int64_t millis() { return micros / 1000; }
    float seconds() { return micros / 1000000; }

    bool    operator< (const TimeDiff &b) { return micros < b.micros;}
    bool    operator> (const TimeDiff &b) { return micros > b.micros;}
};

Uptime& operator+=(      Uptime &a, const Uptime &b);
Uptime& operator-=(      Uptime &a, const TimeDiff &b);

bool    operator==(const TimeDiff &a, const TimeDiff &b);
bool    operator!=(const TimeDiff &a, const TimeDiff &b);

bool    operator> (const TimeDiff &a, const TimeDiff &b);
bool    operator<=(const TimeDiff &a, const TimeDiff &b);
bool    operator>=(const TimeDiff &a, const TimeDiff &b);

bool    operator ==(const Uptime &a, const Uptime &b);
bool    operator !=(const Uptime &a, const Uptime &b);
bool    operator < (const Uptime &a, const Uptime &b);
bool    operator > (const Uptime &a, const Uptime &b);
bool    operator <=(const Uptime &a, const Uptime &b);
bool    operator >=(const Uptime &a, const Uptime &b);

#endif // DF9DCAD8_CC3D_4F7A_B4C7_F1A707FD7367
