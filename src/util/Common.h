/**
 * @brief To be included for any file that needs access to <Arduino.h>.
 *        For testing mode, ARDUINO_CLANG_TESTING must be defined.
 */

#ifndef A47421AB_6587_4DBE_8B3A_ACC7D7FA87F1
#define A47421AB_6587_4DBE_8B3A_ACC7D7FA87F1

#include "VirtualBase.h"
#include <stdio.h> // for size_t

class LineWriter : public VirtualBase {
  public:
    static LineWriter *get();
    static void set(LineWriter*);
    virtual bool operator!() = 0;
    virtual size_t println(const char* a ) = 0;
    virtual size_t println(char a ) = 0;
    virtual size_t println(unsigned char a ) = 0;
    virtual size_t println(int a ) = 0;
    virtual size_t println(unsigned int a ) = 0;
    virtual size_t println(long a ) = 0;
    virtual size_t println(unsigned long a ) = 0;
    virtual size_t println(long long a ) = 0;
    virtual size_t println(unsigned long long a ) = 0;
    virtual size_t println(double a ) = 0;
    virtual size_t println(void) = 0;

    virtual size_t print(const char* a ) = 0;
    virtual size_t print(char a ) = 0;
    virtual size_t print(unsigned char a ) = 0;
    virtual size_t print(int a ) = 0;
    virtual size_t print(unsigned int a ) = 0;
    virtual size_t print(long a ) = 0;
    virtual size_t print(unsigned long a ) = 0;
    virtual size_t print(long long a ) = 0;
    virtual size_t print(unsigned long long a ) = 0;
    virtual size_t print(double a ) = 0;
    virtual size_t print(void) = 0;
};

extern void delay_startup(int delay_tenths);

#include <stdint.h>


#ifndef ARDUINO_CLANG_TESTING
#include <Arduino.h>
#else

#define TEST_SCREEN_OUTPUT "testing/testdata/screens"

// #define __SAMD21G18A__
// #define U8X8_USE_PINS

// these were in my vscode cpp properties file; not sure where i got them or what are required
#define F_CPU 48000000L
#define ARDUINO 10819
#define ARDUINO_SAMD_NANO_33_IOT
#define ARDUINO_ARCH_SAMD
#define CRYSTALLESS
#define USB_VID 0x2341
#define USB_PID 0x8057
#define USBCON
#define USB_MANUFACTURER "Arduino LLC"
#define USB_PRODUCT "Arduino NANO 33 IoT"
#define USBCON

// INPUT conflicts with windows.h (included with catch2)
#define INPUT Arduino_INPUT
// main conflicts with catch2
#define main Arduino_main
// pretend like i know what's happening
#define Arduino_h
#include <api/ArduinoAPI.h>
using namespace arduino;
#undef INPUT
#undef main
#endif


#endif // A47421AB_6587_4DBE_8B3A_ACC7D7FA87F1
