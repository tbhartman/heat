#include "Job.h"

namespace
{
class IntervalFuncJob : public IntervalJob {
    std::function<void()> f;
  public:
    IntervalFuncJob(TimeDiff interval, std::function<void()> f):
        IntervalJob(interval),
        f(f)
    {}
    virtual void run() final {
        f();
    }
};
class DelayedFuncJob : public DelayedJob {
    std::function<void(TimeDiff&)> f;
  public:
    DelayedFuncJob(std::function<void(TimeDiff&)> f):
        DelayedJob(),
        f(f)
    {}
    virtual void run(TimeDiff &nextRun) final {
        f(nextRun);
    }
};
class EveryLoopFuncJob : public EveryLoopJob {
    std::function<void()> f;
  public:
    EveryLoopFuncJob(std::function<void()> f):
        EveryLoopJob(),
        f(f)
    {}
    virtual void run() final {
        f();
    }
};
}
EveryLoopJob *EveryLoopJob::create(std::function<void()> f) {
    return new EveryLoopFuncJob(f);
}
DelayedJob *DelayedJob::create(std::function<void(TimeDiff&)> f) {
    return new DelayedFuncJob(f);
}
IntervalJob *IntervalJob::create(TimeDiff interval, std::function<void()> f) {
    return new IntervalFuncJob(interval, f);
}
