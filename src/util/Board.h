#ifndef CBB7C123_78FC_4D23_AADF_BB8CE86FE27D
#define CBB7C123_78FC_4D23_AADF_BB8CE86FE27D
#include "./Common.h"
#include "VirtualBase.h"
#include "Pin.h"
#include "Uptime.h"

class Board : public VirtualBase
{
    Uptime _uptime;
    unsigned long _lastMicros;
  public:
    Board():
      _uptime{0},
      _lastMicros(0)
    {}
    virtual Pin::DigitalIn setDigitalIn(pin_size_t id, PinMode mode) = 0;
    virtual Pin::DigitalOut setDigitalOut(pin_size_t id, PinStatus initialStatus) = 0;
    /**
     * @brief returns a monotonically increasing time
     */
    virtual Uptime uptime() final;
    virtual void delay(TimeDiff) = 0;
    virtual unsigned long micros() = 0;
};

class RealBoard : public Board
{
  public:
    virtual Pin::DigitalIn setDigitalIn(pin_size_t id, PinMode mode) override;
    virtual Pin::DigitalOut setDigitalOut(pin_size_t id, PinStatus initialStatus) override;
    virtual void delay(TimeDiff) override;
    virtual unsigned long micros() override;
};


#endif // CBB7C123_78FC_4D23_AADF_BB8CE86FE27D
