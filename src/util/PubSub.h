#ifndef F92348E1_75EC_4403_907A_60B43FD34E34
#define F92348E1_75EC_4403_907A_60B43FD34E34

#include "VirtualBase.h"

#include <functional>
#include <map>

class Subscription {
    unsigned long token;
  public:
    Subscription(unsigned long token):
      token(token)
    {}
    unsigned long getToken() const { return token; }
};

template <typename T>
using Subscriber = std::function<void(const T& args)>;

template <typename T>
class Publisher : public VirtualBase {
    unsigned long tokens;
    std::map<unsigned long, Subscriber<T>> subscribers;
    bool enabled;
  public:
    Publisher():
      tokens(0),
      enabled(true)
    {}
    virtual Subscription subscribe(Subscriber<T> f) final {
        Subscription s{tokens++};
        subscribers[s.getToken()] = f;
        return s;
    }
    virtual void unsubscribe(const Subscription& sub) final {
        subscribers.erase(sub.getToken());
    }
    virtual void publish(const T& args) final {
        if (enabled) {
            for (auto& [key,value] : subscribers) {
                value(args);
            }
        }
    }
    virtual void enable() final { enabled = true; }
    virtual void disable() final { enabled = false; }
};
 


#endif // F92348E1_75EC_4403_907A_60B43FD34E34
