#include "Uptime.h"

TimeDiff Uptime::operator-(const Uptime &b)const { return TimeDiff{int64_t(micros) - int64_t(b.micros)};}
    Uptime& Uptime::operator+=(const TimeDiff &b){ micros += b.micros; return *this;}
    Uptime Uptime::operator+ (const TimeDiff &b)const{return Uptime{micros + b.micros};}
