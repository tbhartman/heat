#ifndef D33B0105_2E4D_488B_A98B_30B8231DD6AF
#define D33B0105_2E4D_488B_A98B_30B8231DD6AF

#include "Job.h"
#include "Display/Screen.h"
// namespace Display { class Screen{};}

class ScreenJob : public DelayedJob
{
    Display::Screen* screen;
    bool screenOutOfDate;
    TimeDiff minimumDelay;
  public:
    ScreenJob(Display::Screen* screen, TimeDiff minimumDelay);

    virtual void markScreenOutdated() final;
    virtual void run(TimeDiff &nextRun) final;
    virtual void updateScreen(Display::Screen* s) = 0;

    static ScreenJob *create(Display::Screen* screen, TimeDiff minimumDelay, std::function<void(Display::Screen*)> f);
};


#endif // D33B0105_2E4D_488B_A98B_30B8231DD6AF
