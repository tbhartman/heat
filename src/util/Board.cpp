#include "Board.h"
#include <cassert>

Uptime Board::uptime()
{
  LineWriter::get()->println("Board::uptime");
  LineWriter::get()->println(size_t(this));
  auto us = micros();
  LineWriter::get()->println("got micros");
  _uptime += TimeDiff(us - _lastMicros);
  LineWriter::get()->println("set timediff");
  _lastMicros = us;
  LineWriter::get()->println("return");
  return Uptime{_uptime};
}

Pin::DigitalIn RealBoard::setDigitalIn(pin_size_t id, PinMode mode)
{
#ifdef ARDUINO_CLANG_TESTING
  assert(false);
#endif
  return Pin::DigitalIn(id, mode);
}
Pin::DigitalOut RealBoard::setDigitalOut(pin_size_t id, PinStatus initialStatus)
{
#ifdef ARDUINO_CLANG_TESTING
  assert(false);
#endif
  return Pin::DigitalOut(id, initialStatus);
}
void RealBoard::delay(TimeDiff diff)
{
#ifdef ARDUINO_CLANG_TESTING
  assert(false);
#else
  ::delayMicroseconds((unsigned int)(diff.micros));
#endif
}
unsigned long RealBoard::micros()
{
#ifdef ARDUINO_CLANG_TESTING
  assert(false);
  return 0;
#else
  LineWriter::get()->println("RealBoard::micros");
  auto us = ::micros();
  LineWriter::get()->println("got micros");
  return us;

#endif
}
