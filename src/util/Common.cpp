#include "Common.h"

#ifdef ARDUINO_CLANG_TESTING
#include <vector>
#include <iostream>

static std::vector<PinStatus> pins{};

// include definitions here that need used in testing

void pinMode(pin_size_t pinNumber, PinMode pinMode)
{
    pins.resize(pinNumber);
}

void digitalWrite(pin_size_t pinNumber, PinStatus status)
{
    pins[pinNumber] = status;
}
PinStatus digitalRead(pin_size_t pinNumber)
{
    return pins[pinNumber];
}

class StdoutLineWriter : public LineWriter {
  public:
    bool operator!(){return false;}
    size_t println(const char* a ){ print(a); println(); return 0; }
    size_t println(char a ){ print(a); println(); return 0; }
    size_t println(unsigned char a ){ print(a); println(); return 0; }
    size_t println(int a ){ print(a); println(); return 0; }
    size_t println(unsigned int a ){ print(a); println(); return 0; }
    size_t println(long a ){ print(a); println(); return 0; }
    size_t println(unsigned long a ){ print(a); println(); return 0; }
    size_t println(long long a ){ print(a); println(); return 0; }
    size_t println(unsigned long long a ){ print(a); println(); return 0; }
    size_t println(double a ){ print(a); println(); return 0; }
    size_t println(void){ std::cout << std::endl; return 0; }
    size_t print(const char* a ){ std::cout << a; return 0; }
    size_t print(char a ){ std::cout << a; return 0; }
    size_t print(unsigned char a ){ std::cout << a; return 0; }
    size_t print(int a ){ std::cout << a; return 0; }
    size_t print(unsigned int a ){ std::cout << a; return 0; }
    size_t print(long a ){ std::cout << a; return 0; }
    size_t print(unsigned long a ){ std::cout << a; return 0; }
    size_t print(long long a ){ std::cout << a; return 0; }
    size_t print(unsigned long long a ){ std::cout << a; return 0; }
    size_t print(double a ){ std::cout << a; return 0; }
    size_t print(void){ return 0; }
};

static LineWriter* lw = new StdoutLineWriter{};
LineWriter *LineWriter::get() {
    return lw;
}
void LineWriter::set(LineWriter* newlw) {
    lw = newlw;
}

void delay_startup(int delay_tenths) {}

namespace arduino {
size_t Print::write(unsigned char const*, size_t) {return 0;}
}

#else
void delay_startup(int delay_tenths) {
    pinMode(LED_BUILTIN, OUTPUT);
    bool pin = false;
    for (int i = 0; i < delay_tenths*2; i++)
    {
        digitalWrite(LED_BUILTIN, pin ? HIGH : LOW);
        pin = !pin;
        delayMicroseconds(50000);
    }
    digitalWrite(LED_BUILTIN, LOW);
}
class SerialLineWriter : public LineWriter {
  public:
    bool operator!(){return !Serial;}
    size_t println(const char* a ){ return Serial.println(a); }
    size_t println(char a ){ return Serial.println(a); }
    size_t println(unsigned char a ){ return Serial.println(a); }
    size_t println(int a ){ return Serial.println(a); }
    size_t println(unsigned int a ){ return Serial.println(a); }
    size_t println(long a ){ return Serial.println(a); }
    size_t println(unsigned long a ){ return Serial.println(a); }
    size_t println(long long a ){ return Serial.println(a); }
    size_t println(unsigned long long a ){ return Serial.println(a); }
    size_t println(double a ){ return Serial.println(a); }
    size_t println(void){ return Serial.println(); }

    size_t print(const char* a ){ return Serial.print(a); }
    size_t print(char a ){ return Serial.print(a); }
    size_t print(unsigned char a ){ return Serial.print(a); }
    size_t print(int a ){ return Serial.print(a); }
    size_t print(unsigned int a ){ return Serial.print(a); }
    size_t print(long a ){ return Serial.print(a); }
    size_t print(unsigned long a ){ return Serial.print(a); }
    size_t print(long long a ){ return Serial.print(a); }
    size_t print(unsigned long long a ){ return Serial.print(a); }
    size_t print(double a ){ return Serial.print(a); }
    size_t print(void){ return 0; }
};
class NopLineWriter : public LineWriter {
  public:
    bool operator!(){return false;}
    size_t println(const char* a ){ return 0; }
    size_t println(char a ){ return 0; }
    size_t println(unsigned char a ){ return 0; }
    size_t println(int a ){ return 0; }
    size_t println(unsigned int a ){ return 0; }
    size_t println(long a ){ return 0; }
    size_t println(unsigned long a ){ return 0; }
    size_t println(long long a ){ return 0; }
    size_t println(unsigned long long a ){ return 0; }
    size_t println(double a ){ return 0; }
    size_t println(void){ return 0; }
    size_t print(const char* a ){ return 0; }
    size_t print(char a ){ return 0; }
    size_t print(unsigned char a ){ return 0; }
    size_t print(int a ){ return 0; }
    size_t print(unsigned int a ){ return 0; }
    size_t print(long a ){ return 0; }
    size_t print(unsigned long a ){ return 0; }
    size_t print(long long a ){ return 0; }
    size_t print(unsigned long long a ){ return 0; }
    size_t print(double a ){ return 0; }
    size_t print(void){ return 0; }
};

static LineWriter* lw = nullptr;
LineWriter *LineWriter::get() {
    if ( lw == nullptr ) {
#if 1
        lw = new NopLineWriter{};
#else
        Serial.begin(9600);
        lw = new SerialLineWriter{};
#endif
    } 
    return lw;
}
void LineWriter::set(LineWriter* newlw) {
    lw = newlw;
}

#endif
