#ifndef A5F92EA4_FCDC_40DF_B478_9533B636EACD
#define A5F92EA4_FCDC_40DF_B478_9533B636EACD

#include "Uptime.h"

#include <vector>

class Board;
class TestBoard;
class Job;

class Runner
{
    friend TestBoard;
    Uptime lastLoop;
    bool interrupted;
    std::vector<std::pair<Uptime, Job*>> jobs;
  protected:
    Board* board;
  public:
    Runner(Board* board);

    // interrupt signals that the next loop needs to run innerLoop
    virtual void interrupt() final;

    virtual void loop() final;

    virtual void setup() {};

    void addJob(Job* job);
};

#endif // A5F92EA4_FCDC_40DF_B478_9533B636EACD
