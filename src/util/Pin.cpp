#include "Pin.h"

Pin::DigitalIn::DigitalIn(pin_size_t id, PinMode mode):
    id(id)
{
    pinMode(id, mode);
}
PinStatus Pin::DigitalIn::Read()
{
    return digitalRead(id);
}

Pin::DigitalOut::DigitalOut(pin_size_t id, PinStatus initialStatus):
    id(id)
{
    pinMode(id, OUTPUT);
    Write(initialStatus);
}

void Pin::DigitalOut::Write(PinStatus status)
{
    digitalWrite(id, status);
}
