

/**
 * @brief A virtual base class so I can stick with the rule of zero.
 * 
 * via https://stackoverflow.com/a/41425191/1449189
 */

#ifndef EE2E2119_1701_4AD0_9B01_EC9AD539584F
#define EE2E2119_1701_4AD0_9B01_EC9AD539584F
struct VirtualBase
{
    virtual ~VirtualBase() = default;
    VirtualBase() = default;
    VirtualBase(const VirtualBase&) = default;
    VirtualBase(VirtualBase&&) = default;
    VirtualBase& operator = (const VirtualBase&) = default;
    VirtualBase& operator = (VirtualBase&&) = default;
};


#endif // EE2E2119_1701_4AD0_9B01_EC9AD539584F
