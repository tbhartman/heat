#include "Runner.h"
#include "Board.h"
#include "Uptime.h"
#include "Job.h"

#include <algorithm>
#include <string>

Runner::Runner(Board* board):
  lastLoop(0),
  interrupted(false),
  board(board)
{
}
void Runner::interrupt()
{
  interrupted = true;
}

typedef std::pair<uint64_t, size_t> nextjob;

void Runner::loop()
{
  auto time = board->uptime();
  std::vector<nextjob> torun;
  std::vector<nextjob> torun_next;
  for (size_t i=0; i < jobs.size(); i++)
  {
    if (jobs[i].second->runNext()) {
      torun_next.push_back({0, i});
    } else {
      auto diff = jobs[i].first - time;
      if (diff.micros <= 0) {
        torun.push_back({diff.micros, i});
      }
    }
  }
  struct {
        bool operator()(nextjob a, nextjob b) const { return a.first < b.first; }
  } customLess;
  std::sort(torun.begin(), torun.end(), customLess);
  if (torun.size() > 0) {
    for (auto v: torun_next) {
      jobs[v.second].first = jobs[v.second].second->run(board->uptime());
    }
  }
  for (auto v : torun) {
    time = board->uptime();
    jobs[v.second].first = jobs[v.second].second->run(time);
  }
  time = board->uptime();
  // default to every second
  TimeDiff timeToNext{1000000};
  for (auto& v : jobs)
  {
    if (!v.second->runNext()) {
      auto diff = v.first - time;
      if (diff < timeToNext) {
        timeToNext = diff;
      }
    }
  }
  if (timeToNext.micros > 0) {
    board->delay(timeToNext.micros / 1000);
  }
}

void Runner::addJob(Job* job)
{
  jobs.push_back({0, job});
}
