import argparse
import os
import pathlib
import sys
import json

def getConfig():
    with open('.vscode/c_cpp_properties.json') as f:
        data = json.load(f)
        if len(data['configurations']) == 0:
            raise ValueError("c_cpp_properties must have at least one configuration")
        if len(data['configurations']) == 1:
            return data['configurations'][0]
        for d in data['configurations']:
            if d['name'] == "Arduino":
                return d
        raise ValueError("c_cpp_properties with multiple configurations must have one named 'Arduino'")


def includes():
    c = getConfig()
    print("export CPLUS_INCLUDE_PATH = ", end='')
    os.environ['workspacePath'] = pathlib.Path(__file__).parent.as_posix()
    for path in c['includePath']:
        path = path.replace('\\', '/')
        path = os.path.expandvars(path)
        if "c++" in path or "-gcc/" in path:
            continue
        print(path+';', end='')


def defines():
    c = getConfig()
    for d in c['defines']:
        name = d
        if name.count('='):
            name = name[:name.index('=')]
        if name.count('('):
            continue
        print("#ifndef " + name)
        d = d.replace('=', ' = ', 1)
        print("#define " + d)
        print("#endif")

if __name__ == "__main__":
    p = argparse.ArgumentParser(prog=pathlib.Path(__file__).name)
    p.add_argument('job', metavar="JOB", type=str, nargs=1, help="INCLUDES or DEFINES")

    args = p.parse_args()

    if args.job[0] == "INCLUDES":
        includes()
    elif args.job[0] == "DEFINES":
        defines()
    else:
        print("invalid argument: " + args.job[0])
        sys.exit(1)
