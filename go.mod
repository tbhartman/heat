module gitlab.com/rq3/heatlog

go 1.18

require (
	github.com/google/uuid v1.3.0
	github.com/lithammer/dedent v1.1.0
	github.com/mattn/go-sqlite3 v1.14.15
	github.com/olekukonko/tablewriter v0.0.5
	github.com/sergi/go-diff v1.2.0
	github.com/stretchr/testify v1.4.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/pkg/errors v0.9.1
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/spf13/cobra v1.5.0
)
