#include "src/util/Common.h"
#include "src/util/Board.h"
#include "src/heatlog.h"

#include <U8g2lib.h>

RealBoard* board = new RealBoard{};
HeatLog h{board};

void setup() {
  delay_startup(20);
  h.setup();
}

void loop() {
  h.loop();
}
